import {Component} from "react";
import PropTypes from "prop-types";

class Button extends Component {
    render() {
        const {text, bgColor, click, className} = this.props
        return <button style={{background: bgColor}} className={className} onClick={click}>{text}</button>
    }
}

Button.propTypes = {
    text: PropTypes.string.isRequired,
    bgColor: PropTypes.string,
    click: PropTypes.func.isRequired,
    className: PropTypes.string
}
Button.defaultProps = {
    className: ""
}
export default Button;
