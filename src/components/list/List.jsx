import {Component} from "react";
import Card from "../card/Card";
import PropTypes from "prop-types";
import styles from "./List.module.css"

class List extends Component {


    render() {
        const {data, handleClick, handleFavorite, favorite} = this.props
        return <ul className={styles.wrapper}>{data.map((product) => <Card key={product.id} product={product}
                                                                           isFavorite={favorite.includes(product.id)}
                                                                           handleFavorite={() => handleFavorite(product.id)}
                                                                           handleClick={() => handleClick(product.id)}/>)}</ul>
    }
}

List.propTypes = {
    data: PropTypes.array,
    handleClick: PropTypes.func,
    handleFavorite: PropTypes.func,
    favorite: PropTypes.array
}
export default List;
