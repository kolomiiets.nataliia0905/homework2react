import {Component} from "react";
import {ReactComponent as StarIcon} from "../../img/star-fill.svg"
import {ReactComponent as BasketIcon} from "../../img/basket.svg"
import PropTypes from "prop-types";
import styles from "./Header.module.css"

class Header extends  Component {
    render () {
        const {favLength,cardLength} = this.props
        return <header className={styles.header}><ul className={styles.list}>
            <li className={styles.item}><StarIcon className={styles.starIcon}/>{favLength}</li>
            <li className={styles.item}><BasketIcon className={styles.bascetIcon}/>{cardLength}</li></ul></header>
    }
}
Header.propTypes={favLength:PropTypes.number,cardLength:PropTypes.number}
export default Header;
