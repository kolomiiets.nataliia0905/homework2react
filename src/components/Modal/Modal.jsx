import {Component} from "react";
import './Modal.module.css';
import Button from "../Button/Button";
import styles from "./Modal.module.css"
import PropTypes from "prop-types";
import Header from "../Header/Header";
class Modal extends Component {
    render() {
        const {header, closeButton, text, actions, click} = this.props
        return <div className={styles.wrapper} onClick={click}>
            <div className={styles.modal} onClick={(e) => {e.stopPropagation()}}>
                <h2>{header}</h2>
                {closeButton && <Button className={styles.closeBtn} text={"X"}
                                        click={click}/>}
                <p>{text}</p>
                {actions}

            </div>
        </div>
    }
}
Modal.propTypes={header:PropTypes.string, closeButton:PropTypes.bool, text:PropTypes.string, actions:PropTypes.element, click:PropTypes.func}
export default Modal;