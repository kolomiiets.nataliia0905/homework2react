import {Component} from "react";
import Button from "../Button/Button";
import {ReactComponent as StarIcon} from "../../img/star-fill.svg"
import PropTypes from "prop-types";
import styles from "./Card.module.css"

class Card extends Component {


    render() {
        const {handleClick, product, handleFavorite, isFavorite} = this.props
        const {name, price, image, article} = product
        const starColor = isFavorite ? "red" : "black"

        return <li className={styles.card}><StarIcon className={styles.star} style={{fill: starColor}}
                                                     onClick={handleFavorite}/><img className={styles.img} src={image}
                                                                                    alt={name}/>
            <div><h3>{name}</h3><h4>{article}</h4><h4>{price}</h4><Button click={handleClick} text={"Add to cart"}/>
            </div>
        </li>

    }
}

Card.propTypes = {
    handleClick: PropTypes.func,
    product: PropTypes.object,
    handleFavorite: PropTypes.func,
    isFavorite: PropTypes.bool
}

export default Card;
