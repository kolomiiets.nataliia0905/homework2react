import {Component} from "react";
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import List from "./components/list/List";
import Header from "./components/Header/Header";

class App extends Component {
    state = {products: [],basket: JSON.parse(localStorage.getItem("basket"))||[],favorite:JSON.parse(localStorage.getItem("favorite"))||[], modalProductId: null}
    handleFavorite = (id) => {
        const {favorite}=this.state
        let nextState
        if (favorite.includes(id)) {

            nextState= favorite.filter(productId=>productId!==id)
        } else {
             nextState = [...favorite,id]
        }


        this.setState({favorite:nextState})


        localStorage.setItem("favorite", JSON.stringify(nextState))
    }

    handleClick = (value) => {
        this.setState({modalProductId:value})
    }
    addToBasket = (id) => {
        console.log(this.state)
        const {basket}=this.state
        if (basket.includes(id)) return
        const nextState = [...basket,id]
        this.setState({basket:nextState})
        localStorage.setItem("basket", JSON.stringify(nextState))

    }

componentDidMount(){
        fetch("./product.json").then((res)=>res.json())
            .then((data)=>{ this.setState({products:data})
                console.log(data)})
}
    render() {
        const {products, modalProductId,favorite, basket} = this.state
        return <div className="App">
            <Header favLength={favorite.length} cardLength={basket.length}/>
            {products.length&&<List favorite={favorite} handleFavorite={this.handleFavorite} handleClick={this.handleClick}data={products}/>}



            {modalProductId &&
                <Modal click={() => this.handleClick(null)} header={"Modal1"} closeButton={true}
                       text={"Hello, Max!"}
                       actions={<div className={"button-wrapper"}><Button text={"Ok"} bgColor={"green"}
                                                                          click={() => {
                                                                              this.addToBasket(modalProductId)
                                                                              this.handleClick(null)
                                                                          }}/><Button text={"Cancel"}
                                                                                      bgColor={"green"}
                                                                                      click={() => this.handleClick(null)}/>
                       </div>}/>}


        </div>

    }
}

export default App;
